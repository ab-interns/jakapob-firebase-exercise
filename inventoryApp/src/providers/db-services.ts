import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import * as firebase from 'firebase';

/*
  Generated class for the DbServices provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class DbServices {

fireAuth: any;


  constructor(public http: Http) {
    this.fireAuth = firebase.auth().currentUser.uid;

  }

  createProduct(inventName,id,name){
    return firebase.database().ref(this.fireAuth+'/Inventory/'+inventName+'/Product List').push({
      id: id,
      name: name,
    });
  }

  createInventory(inventName){
    return firebase.database().ref(this.fireAuth+'/Inventory/'+inventName).push({
      name: inventName
    });
  }

  showProduct(productName,items){
    return firebase.database().ref(this.fireAuth+'/Inventory'+'/Product List/'+productName).once('value').then(function(snapshot) {
      items.length = 0;
      snapshot.forEach((e) =>
      {
        var id = e.val().id;
        var name = e.val().name;
        items.push({
            "id": id,
            "name": name
        });
      });
    });
  }

  showInventory(inventName,items){
    return firebase.database().ref(this.fireAuth+'/Inventory').once('value').then(function(snapshot) {
      console.log(snapshot.val());

      var Inname = snapshot.val();
      items.push({
        "name": Inname
      });
    //   items.length = 0;
    //
    //   snapshot.forEach((e) =>
    //   {
    //     var namedb = e.val().name;
    //
    //     items.push({
    //         "name": namedb
    //     });
    // });
    });
  }

}
