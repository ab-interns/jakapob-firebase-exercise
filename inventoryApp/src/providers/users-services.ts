import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';



import * as firebase from 'firebase';

/*
  Generated class for the UsersServices provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class UsersServices {

  public fireAuth: any;

  constructor(public http: Http) {
      this.fireAuth = firebase.auth();
  }

  logoutUser(): any {
    return  firebase.auth().signOut().then(function() {
    // Sign-out successful.
    }, function(error) {
        // An error happened.
    });
  }

  getStarted(){
    return firebase.auth().signInAnonymously().catch(function(error) {
  // Handle Errors here.
    console.log(error.message);
  // ...
    });
  }

  loginUser(email: string, password: string):any {
    return this.fireAuth.signInWithEmailAndPassword(email, password);
  }

  signUpUser(email: string , password: string){
     return this.fireAuth.createUserWithEmailAndPassword(email, password).then((newUser) => {
    //sign in the user
        firebase.database().ref(newUser.uid).child('profile').set({
             email: email
          });
      });
  }

}
