import { Component } from '@angular/core';
import { NavController, NavParams, AlertController,PopoverController } from 'ionic-angular';

import { InventoryPage } from '../inventory/inventory';
import { MypopoverPage } from '../mypopover/mypopover';

import { DbServices } from '../../providers/db-services';

/*
  Generated class for the Home page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [DbServices]
})
export class HomePage {

items: any;
myDate = new Date();
toDayDate: any;
nameInvent: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private dbServices: DbServices,
    private popoverCtrl: PopoverController) {
    this.items = [];
    this.presentInventory();

    var date = this.myDate.getDate();
    var month = this.myDate.getMonth()+1;
    var year = this.myDate.getFullYear();
    this.toDayDate = date+"-"+month+"-"+year;
  }

  presentInventory(){
    this.dbServices.showInventory(this.nameInvent,this.items);
    console.log(this.nameInvent);
  }
  presentPopover() {
      let popover = this.popoverCtrl.create(MypopoverPage);
      popover.present();
    }

 addInventory(){

   let prompt = this.alertCtrl.create({
     title: 'Add Inventory',
     inputs: [
       {
         name: 'inventoryName',
         placeholder: 'Inventory Name'
       },
     ],
     buttons: [
       {
         text: 'Cancel',
         handler: data => {
           console.log('Cancel clicked');
         }
       },
       {
         text: 'Submit',
         handler: data => {
           console.log('Submit clicked');
          //  this.inventName = data.inventoryName;
           //
          //  this.items.push({
          //    name: this.inventName
          //  })
          this.dbServices.createInventory(data.inventoryName).then(() => {
            this.nameInvent = data.inventoryName;
            let alert = this.alertCtrl.create({
              title: 'successful',
              subTitle: "Create successful",
              buttons: ['OK']
            });
            alert.present();
            this.presentInventory();
            // this.items.push({
            //    name: data.inventoryName
            //  })
          });
         }
       }
     ]
   });
   prompt.present();
 }

 itemTapped(event, item) {
   // That's right, we're pushing to ourselves!
   this.navCtrl.push(InventoryPage, {
     nameInventory: item
   });
 }

}
