import { Component } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';

import { ProductPage } from '../product/product';

import { DbServices } from '../../providers/db-services';

/*
  Generated class for the Inventory page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-inventory',
  templateUrl: 'inventory.html',
  providers: [DbServices]
})
export class InventoryPage {

  items: any;
  data: any;

    constructor(public navCtrl: NavController,
      private navParams: NavParams,
      private dbServices:DbServices,
      private alertCtrl:AlertController) {
      this.data = this.navParams.get('nameInventory');

      this.items = [];
      this.presentProduct();
    }

    presentProduct(){
      this.dbServices.showProduct(this.data,this.items);
    }

    redirectProductPage(){
      this.navCtrl.push(ProductPage,{
        nameInventory: this.data
      });
    }

    itemDelete(item){
      var i ;
      for(i = 0; i < this.items.length; i++) {

        if(this.items[i] == item){
          this.items.splice(i, 1);
        }
      }
    }

    itemEdit(item){
      this.navCtrl.push(ProductPage);
    }

}
