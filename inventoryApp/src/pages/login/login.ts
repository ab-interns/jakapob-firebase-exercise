import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';

import { HomePage } from '../home/home';

import { UsersServices } from '../../providers/users-services';
/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [UsersServices]
})
export class LoginPage {

  email: any;
  password: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private usersServices:UsersServices,
    private alertCtrl:AlertController,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController) {

  }

  loginUser() {
    if(this.email || this.password != null){
      this.usersServices.loginUser(this.email, this.password).then(authData => {
          //successful
          let loading = this.loadingCtrl.create({
            content: 'Please wait ....',
            duration: 1000
          });
          loading.present();
          this.navCtrl.setRoot(HomePage);
        }, error => {
          // alert("error logging in: "+ error.message);
          let alert = this.alertCtrl.create({
            title: 'Error loggin in',
            subTitle: error.message,
            buttons: ['OK']
          });
          alert.present();
        });
    }else{
      let alert = this.alertCtrl.create({
        title: 'Error loggin',
        subTitle: 'Please Check you email or password.' ,
        buttons: ['OK']
      });
      alert.present();
    }

  }

  getStart() {
    this.usersServices.getStarted().then(() => {
      this.navCtrl.setRoot(HomePage);
    })
  }

  signUp(){
    let prompt = this.alertCtrl.create({
      title: 'Sign Up',
      inputs: [
        {
          name: 'email',
          placeholder: 'example@mail.com'
        },
        {
          name: 'password',
          placeholder: 'Password'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Submit',
          handler: data => {
            console.log('Submit clicked');

           this.usersServices.signUpUser(data.email, data.password).then(() => {
             let alert = this.alertCtrl.create({
               title: 'successful',

               subTitle: "Create successful",
               buttons: ['OK']
             });
             alert.present();
           });
          }
        }
      ]
    });
    prompt.present();
  }

}
