import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-page1',
  templateUrl: 'page1.html'
})
export class Page1 {

name: any;
  constructor(public navCtrl: NavController,private navParams: NavParams) {
    this.name = navParams.get('name')
    console.log(this.name);
  }

}
