import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

import { InventoryPage } from '../inventory/inventory';

import { DbServices } from '../../providers/db-services';

/*
  Generated class for the Product page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
  providers: [DbServices]
})
export class ProductPage {

  public productID: any;
  public productName: any;
  data: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private dbServices: DbServices,
    private loadingCtrl: LoadingController,
    private alertCtrl:AlertController) {
      this.data = this.navParams.get('nameInventory');
    }

  save() {
    this.dbServices.createProduct(this.data,this.productID,this.productName).then(() => {

      let loading = this.loadingCtrl.create({
        content: 'Please wait ....',
        duration: 1000
      });
      loading.present();
      this.navCtrl.push(InventoryPage,{
        nameInventory: this.data
      });
    }, error => {
      // alert("error logging in: "+ error.message);
      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: error.message,
        buttons: ['OK']
      });
      alert.present();
    })
  }

  edit(){

  }

}
