import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

import { LoginPage } from '../login/login';

import { UsersServices } from '../../providers/users-services';

/*
  Generated class for the Mypopover page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-mypopover',
  templateUrl: 'mypopover.html',
  providers: [UsersServices]
})
export class MypopoverPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private usersServices:UsersServices, private loadingCtrl: LoadingController) {}

  signOut() {
    this.usersServices.logoutUser().then(() =>{
      let loading = this.loadingCtrl.create({
        content: 'Please wait ....',
        duration: 1000
      });
      loading.present();
      this.navCtrl.push(LoginPage); // ผมใช้ setRoot แล้วไม่กลับไปหน้า login ....  whyyyy ?????
    });

  }

}
