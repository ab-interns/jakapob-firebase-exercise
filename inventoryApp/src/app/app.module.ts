import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { Page1 } from '../pages/page1/page1';
import { Page2 } from '../pages/page2/page2';

import { HomePage } from '../pages/home/home';
import { InventoryPage } from '../pages/inventory/inventory';
import { ProductPage } from '../pages/product/product';
import { LoginPage } from '../pages/login/login';
import { MypopoverPage } from '../pages/mypopover/mypopover';

import { DbServices } from '../providers/db-services';
import { UsersServices } from '../providers/users-services';

@NgModule({
  declarations: [
    MyApp,
    Page1,
    Page2,
    HomePage,
    InventoryPage,
    ProductPage,
    LoginPage,
    MypopoverPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Page1,
    Page2,
    HomePage,
    InventoryPage,
    ProductPage,
    LoginPage,
    MypopoverPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler},DbServices,UsersServices]
})
export class AppModule {}
