import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import firebase from 'firebase';

import { Page1 } from '../pages/page1/page1';
import { Page2 } from '../pages/page2/page2';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform) {

    var config = {
      apiKey: "AIzaSyB-IKer5mS0lJW9wERK3_545a6cDQiTKP4",
          authDomain: "inventory-app-d173a.firebaseapp.com",
          databaseURL: "https://inventory-app-d173a.firebaseio.com",
          storageBucket: "inventory-app-d173a.appspot.com",
          messagingSenderId: "145242444154"

    };

   firebase.initializeApp(config);


   var user = firebase.auth().currentUser;
   if(user){

      this.rootPage = HomePage;
     }else{
       this.rootPage = LoginPage;
     }


    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Page One', component: Page1 },
      { title: 'Page Two', component: Page2 }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
